import React from 'react';
import {Row} from 'reactstrap';

const Topnav = () => {
  return(
    <nav className="navbar">
        <Row className="w-100 topnav-wrapper">
          <div className="topnav-content">
            <img src="/color-autodesk-01.png" alt="Autodesk Logo"/>
            <i className="icon-Align-JustifyAll menu-mobile"></i>
            <ul>
              <li className="li-item">FREE TRIALS</li>
              <li className="divider"></li>
              <li className="li-item">ALL PRODUCTS</li>
              <li className="divider"></li>
              <li className="li-item">BUY</li>
            </ul>
          </div>
        </Row>
    </nav>
  )
} 

export default Topnav;