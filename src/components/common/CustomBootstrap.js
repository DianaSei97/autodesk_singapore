import React from 'react';
import { Col } from 'reactstrap';
const Colxx = (props) => (
    <Col {...props} widths={['xxs', 'xs', 'sm', 'md', 'lg', 'xl', 'xxl']} />
);
const Separator = (props) => (
    <div className={`separator ${props.className}`}></div>
);
const StandardColxx = (props) => {
    const defaultProps = { ...props, xxs: '12', md: "6", lg: "6", xl: "4", xxl: "3", className: "mb-4" }
    return <Colxx {...defaultProps} />
};
export { Colxx, Separator, StandardColxx }
