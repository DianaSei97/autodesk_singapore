import React from 'react';
import { Card, CardBody, CardTitle, CardText } from 'reactstrap';
import { StandardColxx } from "./common/CustomBootstrap";

const AutodeskCard = ({icon, title, links, text}) => {
  return(
      <StandardColxx>
        <Card className="autodesk-card">
          <CardBody className="autodesk-card-body">
            <div>
              <CardTitle>
                <i className={`${icon} mr-3`}></i>
                {title}
              </CardTitle>
              <CardText>
                {text}
              </CardText>
            </div>
            <div>
              {links.map((link, id) => {
                return(
                  <div key={id} className="link-wrapper d-inline-flex justify-content-between w-100 pt-2 pb-2">
                    <a href={link.to} className="pb-0">{link.title} </a>
                    <i className="icon-Arrow-Right2"></i>
                  </div>
                )
              })}
            </div>
          </CardBody>
        </Card>
      </StandardColxx>
  )
}

export default AutodeskCard;