import React, { Suspense } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router';
import Topnav from './components/Topnav';
import { createBrowserHistory } from "history";

function App() {
  const Documentation = React.lazy(() =>
    import(/* webpackChunkName: "views-documentation" */ "./views/documentation")
  );
  const Intro = React.lazy(() =>
    import(/* webpackChunkName: "views-intro" */ "./views/intro")
  );
  const Guide = React.lazy(() =>
    import(/* webpackChunkName: "views-guide" */ "./views/guide")
  );
  const Reference = React.lazy(() =>
    import(/* webpackChunkName: "views-reference" */ "./views/reference")
  );
  const history = createBrowserHistory();

  return (
    <Suspense fallback={<div className="loading" />}>
      <Topnav/>
      <div className="app-wrapper">
        <Router history={history}>
          <Switch>
            <Route
              exact
              path="/developer/documentation"
              render={props => <Documentation {...props}/>}/>
            <Route
              exact
              path="/developer/en/api/dummy1/intro"
              render={props => <Intro {...props} />}
            />
            <Route
              exact
              path="/developer/en/api/dummy1/guide"
              render={props => <Guide {...props} />}
            />
            <Route
              exact
              path="/developer/en/api/dummy1/reference"
              render={props => <Reference {...props} />}
            />
            <Redirect to="/error"/>
          </Switch>
        </Router>
      </div>
    </Suspense>
  );
}

export default App;
