import React, { Component } from 'react';
import AutodeskCard from '../components/Card';
import { Row } from 'reactstrap';
import PropTypes from 'prop-types';

class Documentation extends Component{
  state={
    isLoaded: false,
    services: []
  }

  componentDidMount(){
    setTimeout(()=> {
      fetch("/data.json")
      .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoaded: true,
              services: result
            });
          },
          (error) => {
            this.setState({
              isLoaded: true,
              error: error
            });
          })
     }, 1000)
  }

  render(){
    const { isLoaded, services, error } = this.state;
    return(
      isLoaded ? 
        <div>
          <h1 className="mb-0">APIs and Services </h1>
          <p className="mb-5 mt-4">Access infornation on how to use the Forge APIs and Services</p>
          <Row>
            {!error
              ? services.map((item, id) => {
                return(
                  <AutodeskCard 
                    key={id} 
                    icon={item.icon} 
                    title={item.title} 
                    text={item.description} 
                    links={item.links}
                  />
                )
              })
              : <p>oops, something went wrong</p>
            }
          </Row>
        </div>
      : <div className="loading"/>
    )
  }
}

Documentation.propTypes = {
  services: PropTypes.array,
};

export default Documentation;